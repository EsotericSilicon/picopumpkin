# Pico Pumpkin

Pico Pumpkin is an MQTT client and RGB light for CircuitPython devices with Wi-Fi. You will need neopixels and a photoresistor (if you want automatic brightness). I used a Raspberry Pi Pico W, 2x 5mm Neopixel LEDs and a random photoresistor.

This was initially designed as a lit-up dollar store pumpkin with a pico inside, but was repurposed to act as a magnetic door sensor due to being low on picos, and the pumking being near the cabinet I needed to monitor. 


## Features

1. **Web Configurator**: Stored as gzip to conserve RAM. 
2. **Magnetic Door Sensor**: Sends data to your choice of MQTT server.
3. **LED Lights**: Neopixel LEDs for decoration.
4. **Photosensor**: Detects light intensity to adjust brightness of LEDs. 

## Hardware Requirements
- Raspberry Pi Pico W (Or any wifi-enabled circuitpython board, with modifications)
- Dollar store carvable pumpkin (optional)
- Neopixel LEDs (2 by default as "eyes")
- Photosensor
- MQTT Server

## Installation

1. Install CircuitPython on your Raspberry Pi Pico W following the instructions [here](https://learn.adafruit.com/welcome-to-circuitpython/installing-circuitpython-on-raspberry-pi-pico).
2. Copy `main.py`, `index.htm.gz`, `pumpkin.cfg`, the `lib` folder, and the `res` folder to the device. 
3. Remove `code.py` if present, and make sure your wifi name and password are set in `pumpkin.cfg`.


## Usage
1. Connect the photoresistor, the magnetic switch, and the neopixels to the pico.
2. Power up the Raspberry Pi Pico W.
2. Access the web configurator through your web browser.
3. Configure the MQTT server settings.

