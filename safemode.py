import board
import digitalio

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
led.value = True

import neopixel
pixel_pin = board.GP16  # Change this to the pin your NeoPixels are connected to
num_pixels = 1
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=neo_bright, auto_write=True, pixel_order=neopixel.RGB)
  
pixels.fill(0x0000FF)

while True:
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.6)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.6)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.6)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.6)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.6)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.6)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	pixels.fill(0xFF0000)
	led.value = True
	time.sleep(0.3)
	pixels.fill(0x000000)
	led.value = False
	time.sleep(0.3)
	
	time.sleep(1.5)