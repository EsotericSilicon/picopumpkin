import board, digitalio, traceback, gc
#import random
import neopixel
#import ipaddress
import ssl
import wifi
import socketpool
#import adafruit_requests
import time
import asyncio
import re
#import sys
from adafruit_led_animation.animation.sparkle import Sparkle
from adafruit_debouncer import Debouncer
from adafruit_httpserver import Server, Request, Response, GET, Status
from adafruit_httpserver.authentication import (
	AuthenticationError,
	Basic,
	Bearer,
	check_authentication,
	require_authentication,
)
#import busio
#from collections import deque
import json
#import alarm
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import analogio
from digitalio import DigitalInOut, Direction, Pull

cabinet_door_switch = DigitalInOut(board.GP22)
cabinet_door_switch.direction = Direction.INPUT
cabinet_door_switch.pull = Pull.UP


from adafruit_led_animation.animation.colorcycle import ColorCycle
from adafruit_led_animation.color import MAGENTA, ORANGE, TEAL, BLACK, JADE
from adafruit_led_animation.animation.solid import Solid
from adafruit_led_animation.animation.rainbow import Rainbow

# print(f"cpu freq:{microcontroller.cpu.frequency}") #default for pico w is 125000000
# print("attempting overclock...")
# microcontroller.cpu.frequency = 270000000
# print(f"cpu freq:{microcontroller.cpu.frequency}")

storage_rw = True
wifi_error = False
sensor_error_pc = False
mqtt_error = True
neo_color = (0,0,0)
neo_bright = 1
neo_bright_cell = 20000
neo_bright_dim = 0.1
wifi_ssid = ""
wifi_pass = ""
mqtt_host = ""
mqtt_port = 0
mqtt_user = ""
mqtt_pass = ""
server_user = "admin"
server_pass = "scent"

# filename = "index.htm"
# html_content = ""
# with open("index., 'r') as file:
	# # Read the file content and remove newlines and spaces
	# html_content = file.read().replace("\n", "").replace("\r", "").replace("{photocell.value}",str(photocell.value)).replace("{color_to_string(neo_color)}",color_to_string(neo_color)).replace("{neo_bright}",str(neo_bright)).replace("{neo_bright_dim}",str(neo_bright_dim)).replace("{wifi_ssid}",wifi_ssid).replace("{wifi_pass}",wifi_pass).replace("{mqtt_host}",mqtt_host).replace("{mqtt_port}",str(mqtt_port)).replace("{mqtt_user}",mqtt_user).replace("{mqtt_pass}",mqtt_pass).replace("{server_user}",server_user).replace("{server_pass}",server_pass).replace("{ui_storage_error}",ui_storage_error)

with open("index.htm.gz", 'rb') as file:
	html_content = file.read()

def string_to_color(color_string):
	# Ensure the string starts with "#"
	if not color_string.startswith("#"):
		raise ValueError("String should start with '#'")

	# Remove the "0x" prefix and get the RGB components
	color_string = color_string[1:]
	r = int(color_string[0:2], 16)
	g = int(color_string[2:4], 16)
	b = int(color_string[4:6], 16)

	return (r, g, b)

def color_to_string(color_tuple):
	# Extract the RGB components from the tuple
	r, g, b = color_tuple

	# Convert each component to its hexadecimal representation and concatenate them
	color_string = '#{:02x}{:02x}{:02x}'.format(r, g, b)

	return color_string

def load_settings():
	global neo_color, sparkle_0, neo_bright_cell, server_user
	global server_pass, wifi_ssid, wifi_pass, mqtt_host, mqtt_port
	global mqtt_user, mqtt_pass, neo_bright, neo_bright_dim

	try:
		settings_file = open("/pumpkin.cfg", "r")
		settings_raw = settings_file.read()
		settings_file.close()
		settings_data = settings_raw.splitlines()
		print(settings_data)
		neo_color = string_to_color(settings_data[0])
		neo_bright = float(settings_data[1])
		neo_bright_dim = float(settings_data[2])
		neo_bright_cell = int(settings_data[3])
		wifi_ssid = settings_data[4]
		wifi_pass = settings_data[5]
		mqtt_host = settings_data[6]
		mqtt_port = int(settings_data[7])
		mqtt_user = settings_data[8]
		mqtt_pass = settings_data[9]
		server_user = settings_data[10]
		server_pass = settings_data[11]

	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"\r\nLoad error! May be old file, in which case update and save new settings.\r\n{error_details}")

load_settings()

pixel_pin = board.GP18
pixels = neopixel.NeoPixel(pixel_pin, 2, brightness=neo_bright, auto_write=True, pixel_order=neopixel.RGB)

pixels.fill(0x0000FF)

sparkle_0 = Sparkle(pixels, 1, neo_color, 1)
colorcycle = ColorCycle(pixels, 0.5, colors=[neo_color, BLACK])
solidOff = Solid(pixels, color=BLACK)
rainbow = Rainbow(pixels, speed=0.05, period=10)

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT
led.value = True

def save_settings():
	#global neo_color, server_user, server_pass, wifi_ssid
	#global wifi_pass, mqtt_host, mqtt_port, mqtt_user, mqtt_pass
	#global storage_rw, ui_storage_error, neo_bright

	if storage_rw:
		try:
			settings_file = open("/pumpkin.cfg", "w")
			settings_data = f"{color_to_string(neo_color)}\n{neo_bright}\n{neo_bright_dim}\n{neo_bright_cell}\n{wifi_ssid}\n{wifi_pass}\n{mqtt_host}\n{mqtt_port}\n{mqtt_user}\n{mqtt_pass}\n{server_user}\n{server_pass}"
			settings_file.write(settings_data)
			settings_file.close()
		except:
			storage_rw = False
			ui_storage_error = "Connected to PC, storage is read-only!"
			print(ui_storage_error)
	else:
		print(ui_storage_error)

ui_storage_error = ""

pixels.fill(JADE)

#pre-setup pool and server.
pool = socketpool.SocketPool(wifi.radio)

#HTTP setup
server = Server(pool, "/static", debug=True)
# Create a list of available authentication methods.
auths = [
	Basic(server_user, server_pass),
]
server.require_authentication(auths)

def connect_wifi():
	global wifi_error, server
	try:
		pixels.fill(0x0000FF)
		time.sleep(0.5)
		#Setup Wifi
		wifi.radio.hostname = "PicoPumpkin"
		#wifi.radio.hostname = "PicoW"
		print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])
		print("Connecting to %s"%wifi_ssid)
		wifi.radio.connect(wifi_ssid, wifi_pass)
		print("Connected to %s!"%wifi_ssid)
		time.sleep(1)
		print("My IP address is", wifi.radio.ipv4_address)
		pixels.fill(JADE)
		time.sleep(1)
		server.start(str(wifi.radio.ipv4_address))
		wifi_error = False
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"\r\nWifi connect error: \r\n{error_details}")
		pixels.fill(0xFF0000)
		wifi_error = True

connect_wifi()

#MQTT setup

try:
	#Photocell sensor
	photocell = analogio.AnalogIn(board.GP27)
except Exception as ex:
	sensor_error_pc = True

def parse_post_data(data):
	# Convert bytes to string and split key-value pairs
	pairs = data.decode().split('&')
	post_data = {}
	for pair in pairs:
		if '=' in pair:
			key, value = pair.split('=')
			post_data[key] = value
			#print(f"Key:{key} Value: {value}")
	return post_data

_hextobyte_cache = None

def unquote(string):
	"""unquote('abc%20def') -> 'abc def'."""
	global _hextobyte_cache

	if not string:
		return ""

	if isinstance(string, str):
		string = string.encode('utf-8')

	bits = string.split(bytes('%', 'utf-8'))
	if len(bits) == 1:
		return string.decode('utf-8')

	res = [bits[0]]
	append = res.append

	if _hextobyte_cache is None:
		_hextobyte_cache = {}

	for item in bits[1:]:
		try:
			code = item[:2]
			char = _hextobyte_cache.get(code)
			if char is None:
				try:
					char_byte = bytes([int(code, 16)])
					_hextobyte_cache[code] = char_byte
					append(char_byte)
				except ValueError:  # if the code isn't valid hex
					append(bytes('%', 'utf-8') + code)
					item = item[2:]
			else:
				append(char)
			append(item[2:])
		except KeyError:
			append(bytes('%', 'utf-8'))
			append(item)

	value = b"".join(res).decode('utf-8')
	#print(value)
	return value  # Ensure you're returning a decoded string

@server.route("/get-data")
def serve_data(request: Request):
	try:
		#print(f"(get data)Free RAM: {gc.mem_free()} bytes")
		gc.collect()

		# Create a dictionary of all the dynamic content
		data = {
			"photocell_value": str(photocell.value),
			"cabinet_door_switch": str(cabinet_door_switch.value),
			"neo_color": color_to_string(neo_color),
			"neo_bright": str(neo_bright),
			"neo_bright_dim": str(neo_bright_dim),
			"neo_bright_cell": str(neo_bright_cell),
			"wifi_ssid": wifi_ssid,
			"wifi_pass": wifi_pass,
			"mqtt_host": mqtt_host,
			"mqtt_port": str(mqtt_port),
			"mqtt_user": mqtt_user,
			"mqtt_pass": mqtt_pass,
			"server_user": server_user,
			"server_pass": server_pass,
			"ui_storage_error": ui_storage_error
		}

		# Convert the dictionary to a JSON string
		json_data = json.dumps(data)

		headers = {"Content-Type": "application/json; charset=utf-8"}

		#print(f"(get data 2)Free RAM: {gc.mem_free()} bytes")
		return Response(request, json_data, headers=headers)

	except Exception as ex:
		error_details = traceback.format_exception(ex)
		print(f"get-data error:{error_details}") 
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

@server.route("/")
def display_form(request: Request):
	global ui_storage_error, html_content
	try:
		#print(f"(/)Free RAM: {gc.mem_free()} bytes")
		gc.collect()
		headers = {
			"Content-Type": "text/html; charset=utf-8",
			"Content-Encoding": "gzip"
		}
		#print(f"(/2)Free RAM: {gc.mem_free()} bytes")
		return Response(request, html_content, headers=headers)
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

@server.route("/reset")
def reload_cpy(request: Request):
	global ui_storage_error
	import supervisor
	supervisor.reload()

@server.route("/reboot")
def reload_cpy(request: Request):
	import microcontroller
	global ui_storage_error
	microcontroller.reset()

@server.route("/update", methods=["POST"])
def update_values(request: Request):
	global server_user, sparkle_0, neo_bright_cell, colorcycle, server_pass
	global pixels, neo_color, wifi_ssid, wifi_pass, mqtt_host, mqtt_port, mqtt_user
	global mqtt_pass, neo_bright, neo_bright_dim

	try:
		gc.collect()
		pixels.fill(JADE)
		post_data = parse_post_data(request.body)

		if "brightness" in post_data and post_data["brightness"].isdigit():
			try:
				neo_bright = int(post_data["brightness"])
				#print(f"neo_bright:{neo_bright}")
			except ValueError as ex:
				print(f"Error parsing neo_bright: {ex}")

		if "brightness_cell" in post_data and post_data["brightness_cell"].isdigit():
			try:
				neo_bright_cell = int(post_data["brightness_cell"])
				#print(f"neo_bright_cell:{neo_bright_cell}")
			except ValueError as ex:
				print(f"Error parsing brightness_cell: {ex}")

		if "wifi-ssid" in post_data:
			try:
				wifi_ssid = unquote(post_data["wifi-ssid"])
				#print(f"wifi_ssid:{wifi_ssid}")
			except ValueError as ex:
				print(f"Error parsing ssid: {ex}")

		if "wifi-pwd" in post_data:
			try:
				wifi_pass = unquote(post_data["wifi-pwd"])
				#print(f"wifi_pass:{wifi_pass}")
			except ValueError as ex:
				print(f"Error parsing wifi_pass: {ex}")

		if "mqtt-host" in post_data:
			try:
				mqtt_host = unquote(post_data["mqtt-host"])
				#print(f"mqtt_host:{mqtt_host}")
			except ValueError as ex:
				print(f"Error parsing mqtt_host: {ex}")

		if "mqtt-port" in post_data and post_data["mqtt-port"].isdigit():
			try:
				mqtt_port = int(post_data["mqtt-port"])
				#print(f"mqtt_port:{mqtt_port}")
			except ValueError as ex:
				print(f"Error parsing mqtt_port: {ex}")

		if "mqtt-user" in post_data:
			try:
				mqtt_user = unquote(post_data["mqtt-user"])
				#print(f"mqtt_user:{mqtt_user}")
			except ValueError as ex:
				print(f"Error parsing mqtt_user: {ex}")

		if "mqtt-pass" in post_data:
			try:
				mqtt_pass = unquote(post_data["mqtt-pass"])
				#print(f"mqtt_pass:{mqtt_pass}")
			except ValueError as ex:
				print(f"Error parsing mqtt_pass: {ex}")

		if "server-user" in post_data:
			try:
				server_user = unquote(post_data["server-user"])
				#print(f"server_user:{server_user}")
			except ValueError as ex:
				print(f"Error parsing server_user: {ex}")

		if "server-pass" in post_data:
			try:
				server_pass = unquote(post_data["server-pass"])
				#print(f"server_pass:{server_pass}")
			except ValueError as ex:
				print(f"Error parsing server_pass: {ex}")

		if "neo_color" in post_data:
			try:
				neo_color = string_to_color(unquote(post_data["neo_color"]))
				#print(f"neo_color:{neo_color}")
			except ValueError as ex:
				print(f"Error parsing neo_color: {ex}")

		# To handle floats, we use a regex pattern to match valid float and integer inputs

		float_pattern = r"^\d+(\.\d+)?$"

		if "brightness" in post_data and re.match(float_pattern, post_data["brightness"]):
			try:
				brightness = float(post_data["brightness"])
			except ValueError:
				pass
		if "brightness_dim" in post_data and re.match(float_pattern, post_data["brightness_dim"]):
			try:
				neo_bright_dim = float(post_data["brightness_dim"])
			except ValueError:
				pass

		sparkle_0 = Sparkle(pixels, 1, neo_color, 1)

		colorcycle = ColorCycle(pixels, 0.5, colors=[neo_color, BLACK])
		save_settings()

		# Redirect to the root
		headers = {"Location": "/"}
		return Response(request, "", status=Status(302, "found"), headers=headers)

	except Exception as ex:
		error_details = traceback.format_exception(ex)
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

def get_content_type(filename):
	if filename.endswith('.ico'):
		return 'image/x-icon'
	elif filename.endswith('.png'):
		return 'image/png'
	elif filename.endswith('.jpg') or filename.endswith('.jpeg'):
		return 'image/jpeg'
	elif filename.endswith('.txt'):
		return 'text/plain'
	else:
		return 'application/octet-stream'  # generic binary data

@server.route("/res/<path>")
def serve_resource(request: Request, path: str):
	#print(f"(res)Free RAM: {gc.mem_free()} bytes")
	gc.collect()
	# Determine the real file path
	file_path = "/res/" + path
	print(f"Getting file {file_path}")
	try:
		gc.collect()
		# Open and read the file
		with open(file_path, 'rb') as file:
			content = file.read()

		# Set headers
		headers = {
			'Content-Type': get_content_type(path),
			'Content-Length': str(len(content))
		}

		#print(f"(res2)Free RAM: {gc.mem_free()} bytes")
		return Response(request, content, headers=headers)

	except OSError:
		return Response(request, "File not found", status=404)
	except Exception as ex:
		error_details = traceback.format_exception(ex)
		return Response(request, f"An error occurred: {ex}\n\nDetails:\n{error_details}")

async def main_loop():
	global	neo_id

	while True:
		try:

			#print(f"(gc loop)Free RAM: {gc.mem_free()} bytes")
			gc.collect()
			#print(f"(gc loop 2)Free RAM: {gc.mem_free()} bytes")
			await asyncio.sleep(0.3)
		except Exception as ex:
			error_details = traceback.format_exception(ex)
			print(f"\r\nMain Loop/Sensor Error!\r\n{error_details}")
			await asyncio.sleep(0.3)

async def neopixel_loop():
		global pixels, photocell, neo_id, colorcycle_good, wifi_error, sparkle_0, colorcycle, solidOff
		#timer_blink_off = time.monotonic() + random.uniform(0.3,8)
		#timer_blink_on = timer_blink_off + 0.2
		while True:
			#if time.monotonic() < timer_blink_off:
			#sparkle_0.animate()
			#elif time.monotonic() >= timer_blink_off:
			#	solidOff.animate()
			#elif time.monotonic() >= timer_blink_on:
			#	timer_blink_off = time.monotonic() + random.uniform(0.3,8)
			#	timer_blink_on = timer_blink_off + 0.2

			if neo_color == (0,0,0):
				rainbow.animate()
			else:
				sparkle_0.animate()

			if (photocell.value > neo_bright_cell):
				pixels.brightness = neo_bright
			else:
				pixels.brightness = neo_bright_dim

			await asyncio.sleep(0.01)

async def wifi_loop():
	global wifi_error, neo_id
	while True:
		if wifi_error:
			connect_wifi()
			await asyncio.sleep(3)
		else:
			try:
				server.poll()
				await asyncio.sleep(0.3)
			except Exception as ex:
				error_details = traceback.format_exception(ex)
				print(f"Wifi error...reconnecting... {error_details}")
				wifi_error = True
				await asyncio.sleep(5)

async def mqtt_loop():
	global ccs, photocell, cabinet_door_switch, smell_state, avg_tvoc, mqtt_error, mqtt_client, neo_id, pool
	mqtt_client = MQTT.MQTT(
			broker=mqtt_host,
			port=mqtt_port,
			username=mqtt_user,
			password=mqtt_pass,
			socket_pool=pool,
			#ssl_context=ssl.create_default_context(),
		)

	MQTT_TOPIC = "state/PicoPumpkin"

	while True:
		try:
			if mqtt_error:
				print("Attempting to connect to %s" % mqtt_client.broker)
				mqtt_client.connect()
				mqtt_error = False
				#await asyncio.sleep(60)
			else:
				#char_array_state = list(smell_state)

				print("Publishing to %s" % MQTT_TOPIC)
				output = {
					"PHOTOCELL": photocell.value,
					"COLOR": color_to_string(neo_color),
					"CABINET_DOOR_SWITCH": str(cabinet_door_switch.value)
				}
				mqtt_client.publish(MQTT_TOPIC, json.dumps(output))
			await asyncio.sleep(0.5)
		except Exception as ex:
			error_details = traceback.format_exception(ex)
			print(f"MQTT error...reconnecting... {error_details}")
			mqtt_error = True
			neo_id = 9
			await asyncio.sleep(5)

async def main():
	main_task = asyncio.create_task(main_loop())
	neopixel_task = asyncio.create_task(neopixel_loop())
	wifi_task = asyncio.create_task(wifi_loop())
	mqtt_task = asyncio.create_task(mqtt_loop())
	await asyncio.gather(main_task,wifi_task,neopixel_task,mqtt_task)

asyncio.run(main())